<?php

namespace wallesoft\easy\rest\module\models;

use easy;
use easy\helpers\Helper;
class Api{

    /**
     * 接口类的文件路径
     * @var string
     */
    public $apiPath ;

    /**
     * api接口类 命名空间
     * @var string
     */
    public $namespace;
    /**
     * 接口名称
     * @var string
     */
    public $name;
    /**
     * 设置那个目录生成接口文件
     * @param string $path 文件目录
     */
    public function config($name, $path, $namespace){
      $this->apiPath  = $path;
      $this->namespace = $namespace;
      $this->name = $name;
    }

    public function format(){
      //获取文件列表
      $files = Helper::readFileList($this->apiPath);
      return $this->formatClass($files);

    }
    public function formatClass($files){
      $allApis = [];
      foreach($files as $file){

        $apiClass = str_replace([$this->apiPath.DIRECTORY_SEPARATOR, 'Controller.php'],['', ''], $file);
        $apiClassShortName = str_replace(DIRECTORY_SEPARATOR, '_', $apiClass);
        $apiClassName = $this->namespace. '\\' . str_replace('_', '\\', $apiClassShortName) . 'Controller';
        if(!class_exists($apiClassName)) {
          continue;
        }
        // 左侧标题 菜单内容等
        $ref = new \ReflectionClass($apiClassName);
        $title = "[ 请检查接口文件注释 {{$apiClassName}}]";
        $desc = "[ 请使用 @desc 注释说明 ]";
        $isClassIgnore = false; //是否屏蔽此接口
        $docComment = $ref->getDocComment();
        if($docComment !== false){
          $docCommentArr = explode("\n", $docComment);//通过回车分割成数组
          $comment = trim($docCommentArr[1]);
          $title = trim(substr($comment, strpos($comment, '*')+1));
          foreach($docCommentArr as $comment) {
            $pos = stripos($comment, '@desc');
            if($pos !== false) {
              $desc = substr($comment, $pos+5);
            }
            // ignore or not
            if(stripos($comment, '@ignore') !== false){
              $isClassIgnore = true;
            }
          }
        }
        if($isClassIgnore){
          continue;
        }
        // 格式数据
        $allApis[$this->name][$apiClassShortName]['title'] = $title;
        $allApis[$this->name][$apiClassShortName]['desc'] = $desc;
        //获取 method
        $allApis[$this->name][$apiClassShortName]['methods'] = [];
        $methodRefs = array_diff(get_class_methods($ref->getName()), get_class_methods($ref->getParentClass()->getName()));

        foreach($methodRefs as $method) {
          $methodRef = $ref->getMethod($method);
          if(!$methodRef->isPublic() || strpos($methodRef->getName(), '__') === 0) {
            continue;
          }
          $title = "[检查接口函数注释-{$methodRef->getName()}]";
          $desc = "[使用@desc对函数进行描述]";
          $isMethodIgnore = false;
          $docComment = $methodRef->getDocComment();
          if($docComment !== false) {
            $docCommentArr = explode("\n", $docComment);
            $comment = trim($docCommentArr[1]);
            $title = trim(substr($comment, strpos($comment, '*') + 1));

            //desc
            foreach($docCommentArr as $comment) {
              $pos = stripos($comment,'@desc');
              if($pos !== false) {
                $desc = substr($comment, $pos+5);
              }

              if(stripos($comment,'@ignore') !== false) {
                $isMethodIgnore = true;
              }

            }
          }
          if($isMethodIgnore) {
            continue;
          }
          // 去除action 前缀
          $service = $this->name .'.'. $apiClassShortName . '.' .ucfirst(substr($methodRef->getName(), 6));
          $allApis[$this->name][$apiClassShortName]['methods'][$service] = [
            'service' => $service,
            'title' => $title,
            'desc' => $desc,
          ];
        }
      }
      return $allApis;
    }
    public function formatAction($apiClass, $action){

      $className = '\\' . $this->namespace . '\\' . ucfirst($apiClass) . 'Controller';
      $descComment = "[请使用@desc 注释]";


    }
}
