<?php
namespace wallesoft\easy\rest\module\controllers;

use easy;
use easy\base\Controller;
use wallesoft\easy\rest\module\models\ApiDesc;

class ServiceController extends Controller
{
    public function actionIndex() {
       //获取参数

       $service = easy::$app->request->getQueryParam('s');
       list($app, $className, $action) = $this->formatService($service);
       $requestUrl = easy::$app->url->to([lcfirst($className).'/'.lcfirst($action)]);
       $className = 'app\\controllers\\'.ucfirst($className).'Controller';


       $rules = [];
       $returns = [];
       $descComment = '//请使用@desc 注释';
       $exception = [];
       $rules = $this->getRules($className, lcfirst($action));
       $action = 'action'.ucfirst($action);
       $refClass = new \ReflectionClass($className);

       $classDocComment = $refClass->getDocComment();

       while($parent = $refClass->getParentClass()) {
           if($parent->getName() == 'easy\\rest\Controller') {
               break;
           }
           $classDocComment = $parent->getDocComment(). "\n" .$classDocComment;
           $refClass = new \ReflectionClass($parent->getName());
       }
      $needClassDocComment = '';
      foreach(explode("\n", $classDocComment) as $comment) {
          if (stripos($comment, '@exception') !== FALSE
                || stripos($comment, '@return') !== FALSE) {
                $needClassDocComment .=  "\n" . $comment;
            }
      }

      $refMethod = new \reflectionMethod($className,$action);
      $docCommentArr = explode("\n", $needClassDocComment . "\n" . $refMethod->getDocComment());

        foreach ($docCommentArr as $comment) {
            $comment = trim($comment);

            //标题描述
            if (empty($description) && strpos($comment, '@') === FALSE && strpos($comment, '/') === FALSE) {
                $description = substr($comment, strpos($comment, '*') + 1);
                continue;
            }

            //@desc注释
            $pos = stripos($comment, '@desc');
            if ($pos !== FALSE) {
                $descComment = substr($comment, $pos + 5);
                continue;
            }

            //@exception注释
            $pos = stripos($comment, '@exception');
            if ($pos !== FALSE) {
                $exArr = explode(' ', trim(substr($comment, $pos + 10)));
                $exceptions[$exArr[0]] = $exArr;
                continue;
            }

            //@return注释
            $pos = stripos($comment, '@return');
            if ($pos === FALSE) {
                continue;
            }

            $returnCommentArr = explode(' ', substr($comment, $pos + 8));
            //将数组中的空值过滤掉，同时将需要展示的值返回
            $returnCommentArr = array_values(array_filter($returnCommentArr));
            if (count($returnCommentArr) < 2) {
                continue;
            }
            if (!isset($returnCommentArr[2])) {
                $returnCommentArr[2] = '';	//可选的字段说明
            } else {
                //兼容处理有空格的注释
                $returnCommentArr[2] = implode(' ', array_slice($returnCommentArr, 2));
            }

            //以返回字段为key，保证覆盖
            $returns[$returnCommentArr[1]] = $returnCommentArr;
        }

        include(easy::$app->view->findFile('/api_desc'));

    }

    public function formatService($service) {
        return explode('.', $service);
    }
    public function getRules($class,$action) {
        $rules = (new $class($this->id,$this->module))->rules();
        if(!isset($rules[$action])) {
             return [];
        }

        $rules = $rules[$action];
        $newRules = [];
        foreach($rules as $rule) {
            if(in_array('required', $rule) && is_array($rule[0])){
                //required 较为特殊单独处理
                foreach($rule[0] as $ru) {
                    $newRules[$ru]['name'] = $ru;
                    $newRules[$ru]['require'] = true;
                }
            } elseif(in_array('required', $rule)){
                $newRules[$rule[0]]['name'] = $rule[0];
                $newRules[$rule[0]]['require'] = true;

            } else {
                $newRules[$rule[0]]['name'] = $rule[0];
                $newRules[$rule[0]]['type'] = $rule[1];
                $newRules[$rule[0]] = array_merge($newRules[$rule[0]], $rule);
            }

        }

        return $newRules;
    }
}
