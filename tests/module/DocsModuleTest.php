<?php

use PHPUnit\framework\TestCase;

use easy\web\Application;
use wallesoft\easy\rest\module\DocsModule;
class DocsModuleTest extends TestCase{
  //dirname(__FILE__.'/../../module/controller'
  public function testGetProjectDir(){
    $config = [
      'appPath'=>dirname(__FILE__).'/../',
      'modules'=>[
        'docs'=>[
        'class'=>'wallesoft\easy\rest\module\DocsModule',
        'projectDir'=>'testDir'
        ]
      ]
    ];
    $app = new Application($config);

    $docModule = $app->getModule('docs');
    $this->assertSame('testDir',$docModule->getProjectDir());
  }
}
