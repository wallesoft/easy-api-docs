<?php

/**
 * phpunit bootstrap
 */
//load vendor autoload
require_once dirname(__FILE__).'/../vendor/autoload.php';
//require easy
require_once dirname(__FILE__).'/../vendor/wallesoft/easy/framework/Easy.php';
//require docs module
require_once dirname(__FILE__).'/../module/DocsModule.php';
