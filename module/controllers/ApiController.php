<?php

namespace wallesoft\easy\rest\module\controllers;

use easy;
use easy\base\Controller;
use wallesoft\easy\rest\module\models\Api;
class ApiController extends Controller{


  public function actionIndex(){
    //主题风格
    $theme = $_GET['type'] !== null ? $_GET['type'] : 'fold';
    $this->output('theme',$theme);
    $module = $this->module;
    $apiModel = new Api();
    $apiModel->config($module->apiName,$module->projectDir,'app\controllers');
    $allApis = $apiModel->format();
    $this->output('baseUrl',$module->resourceUrl);
    $this->output('allApis',$allApis);
    $this->output('projectName', $module->apiName);
    $this->renderPartial ('/api_list');
  }

  public function actionService(){
    //print_r(easy::$app->request->getQueryParam('s'));
    $service = easy::$app->request->getQueryParam('s');
    list($apiName, $apiClassShortName, $action) = $service;
    $apiModel = new Api();
    $module = $this->module;
    $apiModel->config($apiName,$module->projectDir,'app\controllers');
    $apiDesc = $apiModel->formatAction($apiClassShortName,$action);
  }
}
