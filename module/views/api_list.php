<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo $output['projectName']; ?> - 在线接口列表</title>
    <link href="<?php echo $output['baseUrl'];?>/vendor/wallesoft/easy-api-docs/static/semantic.min.css" rel="stylesheet">
    <script src="<?php echo $output['baseUrl'];?>/vendor/wallesoft/easy-api-docs/static/jquery.min.js"></script>
    <script src="<?php echo $output['baseUrl'];?>/vendor/wallesoft/easy-api-docs/static/semantic.min.js"></script>
    <meta name="robots" content="none"/>
</head>
<body>

<?php
$table_color_arr = explode(" ", "red orange yellow olive teal blue violet purple pink grey black");
$allApiS = $output['allApis'];
?>

<div class="ui text container" style="max-width: none !important; width: 1200px" id="menu_top">

  <div class="ui floating message">
        <div class="ui grid container" style="max-width: none !important;">
            <?php
            if ($output['theme'] == 'fold') {
            ?>
            <div class="four wide column">
                <div class="ui vertical accordion menu">
                <?php
                    // 总接口数量
                    $methodTotal = 0;
                    foreach ($allApiS as $namespace => $subAllApiS) {
                        foreach ($subAllApiS as $item) {
                            $methodTotal += count($item['methods']);
                        }
                    }
                ?>
                    <div class="item"><h4>接口服务列表&nbsp;(<?php echo $methodTotal; ?>)</h4></div>

                <?php
                    $num = 0;
                    foreach ($allApiS as $namespace => $subAllApiS) {
                        echo '<div class="item">';
                        $subMethodTotal = 0;
                        foreach ($subAllApiS as $item) {
                            $subMethodTotal += count($item['methods']);
                        }
                        echo sprintf('<h4 class="title active" style="font-size:16px;margin:0px;"><i class="dropdown icon"></i>%s (%d)</h4>', $namespace, $subMethodTotal);
                        echo sprintf('<div class="content %s" style="margin-left:-16px;margin-right:-16px;margin-bottom:-13px;">', $num == 0 ? 'active' : '');
                        // 每个命名空间下的接口类
                        foreach ($subAllApiS as $key => $item) {
                            echo sprintf('<a class="item %s" data-tab="%s">%s</a>', $num == 0 ? 'active' : '', $key, $item['title']);
                            $num++;
                        }
                        echo '</div></div><!-- END OF NAMESPACE -->';
                    } // 每个命名空间下的接口

                    ?>
                    <div class="item">
                        <div class="content" style="margin:-13px -16px;">
                            <a class="item" href="#menu_top">返回顶部↑↑↑</a>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?> <!-- 折叠时的菜单 -->

            <!-- 折叠时与展开时的布局差异 -->
            <?php if ($output['theme'] == 'fold') { ?>
            <div class="twelve wide stretched column">
            <?php } else { ?>
            <div class="wide stretched column">
            <?php
                    // 展开时，将全部的接口服务，转到第一组
                    $mergeAllApiS = array('all' => array('methods' => array()));
                    foreach ($allApiS as $namespace => $subAllApiS) {
                        foreach ($subAllApiS as $key => $item) {
                            if (!isset($item['methods']) || !is_array($item['methods'])) {
                                continue;
                            }
                            foreach ($item['methods'] as $mKey => $mItem) {
                                $mergeAllApiS['all']['methods'][$mKey] = $mItem;
                            }
                        }
                    }
                    $allApiS = array('ALL' => $mergeAllApiS);
            }
            ?>
                <?php
                $uri  = !$env ? substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], '?')) : '';
                $num2 = 0;
                foreach ($allApiS as $namespace => $subAllApiS) {
                foreach ($subAllApiS as $key => $item) {
                    ?>
                    <div class="ui  tab <?php if ($num2 == 0) { ?>active<?php } ?>" data-tab="<?php echo $key; ?>">
                        <table
                            class="ui red celled striped table <?php echo $table_color_arr[$num2 % count($table_color_arr)]; ?> celled striped table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>接口服务</th>
                                <th>接口名称</th>
                                <th>更多说明</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php
                            $num = 1;
                            foreach ($item['methods'] as $mKey => $mItem) {

                                    $concator = strpos($uri, '?') ? '&' : '?';

                                    $link = easy::$app->url->to(['docs/api/service', 's'=>$mItem['service']]);
                                    //$uri . $concator . 'service=' . $mItem['service'] . '&detail=1' . '&type=' . $theme;
                                    $link = $uri . $concator . 'r=docs/service&s=' . $mItem['service'] . '&detail=1' . '&type=' . $theme;


                                $NO   = $num++;
                                echo "<tr><td>{$NO}</td><td><a href=\"$link\" target='_blank'>{$mItem['service']}</a></td><td>{$mItem['title']}</td><td>{$mItem['desc']}</td></tr>";
                            }
                            ?>
                            </tbody>
                        </table>

                    <!-- 主题切换，仅当在线时才支持 -->
                    <?php


                                if ($output['theme']== 'fold') {
                                    echo '<div style="float: right"><a href="' . easy::$app->url->to(['docs','type'=>'expand']). '">切换回展开版</a></div>';
                                } else {
                                    echo '<div style="float: right"><a href="' .easy::$app->url->to(['docs','type'=>'fold']) . '">切换回折叠版</a></div>';
                                }

                    ?>

                    </div>
                    <?php
                    $num2++;
                } // 单个命名空间的循环
                } // 遍历全部命名空间
                ?>


            </div>
        </div>
        <div class="ui blue message">
            <strong>温馨提示：</strong> 此接口服务列表根据后台代码自动生成，可在接口类的文件注释的第一行修改左侧菜单标题。
        </div>

    </div>
    </div>
</div>
<script type="text/javascript">
    $('.accordion.menu a.item').tab({'deactivate':'all'});
    $('.ui.sticky').sticky();
	//当点击跳转链接后，回到页面顶部位置
    $(".accordion.menu a.item").click(function() {
        $('body,html').animate({
                scrollTop: 0
            },
            500);
        return false;
    });

    $('.ui.accordion').accordion({'exclusive':false});

</script>

</body>
</html>
<?php
if ($env){
    $string = ob_get_clean ();
    \PhalApi\Helper\saveHtml ($webRoot, 'index', $string);
    $str = "
Usage:

生成展开版：  php {$argv[0]} expand
生成折叠版：  php {$argv[0]} fold

脚本执行完毕！离线文档保存路径为：";
    if (strtoupper ( substr ( PHP_OS, 0,3)) == 'WIN'){
        $str = iconv ( 'utf-8', 'gbk', $str);
    }
    $str .= $webRoot . D_S . 'docs' ;
    echo $str, PHP_EOL, PHP_EOL;
    exit(0);
}
