<?php

namespace wallesoft\easy\rest\module;

use easy;
use easy\base\Module;
use easy\helpers\Exception;
class DocsModule extends Module{
  //default controller
  public $defaultRoute = 'api';
  //配置需要文档的的地址
  public $projectDir;
  // 配置接口目录 默认 Api
  public $apiName = 'Api';
  // 接口版本
  public $apiVersion = '';
  // 静态文件 js  css 加载地址配置路径
  public $resourceUrl = '/vendor/wallesoft/easy-api-docs';
  public function init()
  {
      parent::init();
      //$this->resourceUrl = easy::$app->urlManager->getBaseUrl();
      if($this->projectDir === null){
       Exception::throwException("module docs need projectDir config");
      }
  }
  public function getProjectDir(){
    return $this->projectDir;
  }
}
