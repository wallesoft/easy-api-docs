# easy-api-docs

#### 项目介绍
此module是rest接口文档显示

easy-api-docs 是一个easy framework 示例扩展，用于接口开发时的接口文档展示

### Installlation

### use composer.
- 在项目中 `composer.json` 文件中添加依赖：

```json
"require":{
  "wallesoft/easy-api-docs":"*"
}
```
- 执行 `$ php composer.phar update` 或 `$ composer update` 安装

- 配置：在应用的配置文件中（配置文件路径 your_app_directory/config/main.php）添加配置到module中

然后配置文件中添加配置信息在module中
```php

'modules'=> [
  'docs'=> [
    'class'=>'wallesoft\easy\rest\module\DocsModule',
    'projectDir'=> dirname(__FILE__).'/../controllers',
    'apiName' => 'App',
  ]

```

### Usage

- 查看在线文档

http://your_domain.com/index.php?r=docs //总接目录列表 启动 `r=docs` 为在module配置中的名称
http://your_domain.com/index.php?r=docs&service=api/index //详细接口页面

访问可以通过url重写优化访问方式




#### Todo list

#### done

1.接口分类列表页显示

2.接口详细参数及调试

#### maybe

1.命令行模式生成本地文档
